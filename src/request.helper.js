
const safeParseNumber = (value) => {
    if (typeof value === 'string' && value.length > 16) {
        // string larger than MAX_SAFE_INTEGER
        throw Error('String parameter too large');
    }
    const finalValue = Number(value);
    if (Number.isNaN(finalValue) || finalValue > Number.MAX_SAFE_INTEGER) {
        throw Error('Incorrect value');
    }
    return finalValue;
};

const getQueryParam = (req, key) => {
    if (!req || !req.query || !req.query.hasOwnProperty(key)) {
        throw Error('Missing parameter')
    }
    return req.query[key];
};

const getBodyParam = (req, key) => {
    if (!req || !req.body || !req.body.hasOwnProperty(key)) {
        throw Error('Missing body parameter')
    }
    return req.body[key];
};

const getBodyObject = (req) => {
    if (!req || !req.body) {
        throw Error('Missing body parameter')
    }
    if (typeof req.body !== 'object') {
        throw Error('Body not an object')
    }
    return req.body;
}

const getNumberQueryParam = (req, key) => {
    let value = getQueryParam(req, key);
    return safeParseNumber(value);
};

module.exports = {
    getQueryParam,
    getNumberQueryParam,
    safeParseNumber,
    getBodyParam,
    getBodyObject,
};
