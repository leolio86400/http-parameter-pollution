'use strict';

// requirements
const express = require('express');
const payment = require('./payment');
const {
    getQueryParam,
    getNumberQueryParam,
} = require('./request.helper');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    try {
        const action = getQueryParam(req, 'action');
        const amount = getNumberQueryParam(req, 'amount');
        // We only allow transfer
        if(action === 'transfer') {
            return res.send(payment(action, amount));
        }
        return res.status(400).send('You can only transfer an amount');
    } catch(err) {
        console.log(err);
        if (err && err.name === 'ValidationError') {
            return res.status(400).send(err.message);
        }
        return res.status(400).send(err.message);
    }
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
