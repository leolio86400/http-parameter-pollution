
class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = "ValidationError";
    }
}

const validateNumber = (value, min, max) => {
    if (typeof value !== 'number' || Number.isNaN(value)) {
        throw new ValidationError('Incorrect value');
    }
    if (value < min || value > max) {
        throw new ValidationError('Incorrect value');
    }
    return true;
};

const validateString = (value, min, max, rgx) => {
    if (typeof value !== 'string') {
        throw new ValidationError('Wrong parameter');
    }
    if (!value.trim()) {
        throw new ValidationError('Value is empty');
    }
    if (value.length < min || value.length > max) {
        throw new ValidationError('Value does not match requirements');
    }
    if (!!rgx && !new RegExp(rgx, 'gi').test(value)) {
        throw new ValidationError('Value does not pass validation');
    }
    return true;
};

const validateEmail = (value) => {
    return validateString(
        value,
        5,
        320,
        '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
    );
};

const validateStringInValues = (value, authorizedValues) => {
    if (!authorizedValues || authorizedValues.length <= 0) {
        throw Error('Please specify authorizedValues');
    }
    // copy and sort values by size
    const authorizedValuesSorted = authorizedValues
        .slice()
        .sort((a, b) => a.length - b.length);
    // validate using the smallest and longest size
    validateString(
        value,
        authorizedValuesSorted[0].length,
        authorizedValuesSorted[authorizedValuesSorted.length - 1].length,
    );
    if (!authorizedValuesSorted.includes(value)) {
        throw new ValidationError('Incorrect value');
    }
    return true;
};

module.exports = {
    ValidationError,
    validateNumber,
    validateString,
    validateEmail,
    validateStringInValues,
};
