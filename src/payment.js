const {
    validateStringInValues,
    validateNumber,
} = require("./validators.helper");

class Payment {
    constructor(action, amount) {
        validateStringInValues(action, ['transfer', 'withdraw']);
        validateNumber(amount, 0, Number.MAX_SAFE_INTEGER);
        this.action = action;
        this.amount = amount;
        Object.freeze(this);
    }
}

function makePayment(action, amount) {
    const payment = new Payment(action, amount);
    if (action === 'withdraw') {
        return `Successfully withdrew $${payment.amount}`;
    } else if (action === 'transfer') {
        return `Successfully transferred $${payment.amount}`;
    }
    return 'You must specify an action: withdraw or transfer';
}

module.exports = makePayment;
